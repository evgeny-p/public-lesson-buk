import random


secret_number = random.randint(0, 100)
print(secret_number)
while True:
    user_guess = int(input("Guess! "))

    if user_guess == secret_number:
        print("You are right!")
        break
    elif user_guess < secret_number:
        print(f'My number > {user_guess}')
    elif user_guess > secret_number:
        print(f'My number < {user_guess}')
