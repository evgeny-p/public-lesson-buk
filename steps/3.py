my_friend = "James"
user_name = input("Username: ")

if user_name == my_friend:
    print(f"Hello, my old friend {user_name}!")
else:
    print(f"Hello, {user_name}")
