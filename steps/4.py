import random


secret_number = random.randint(0, 100)

user_guess = int(input("Guess! "))
if user_guess == secret_number:
    print("You are right!")
else:
    print("No...")
